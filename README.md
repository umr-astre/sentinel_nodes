# Sentinel_Nodes





# Description

This repository contains the main R-functions developed to identify potential sentinel nodes on an evolving network and presented in the article [Description of Cattle and Small Ruminants trade network inSenegal and implication for the surveillance of animal diseases](https://doi.org/10.48550/arXiv.2301.11784) submitted to [Transboundary and Emerging Diseases](https://onlinelibrary.wiley.com/journal/18651682)

## Background
In West Africa, livestock mobility is an intrinsic component of livestock production and trade. The harsh environmental conditions, as well as the absence of the facilities required to slaughter animals and store meat, implies that the livestock has to be mobile and the majority of the animals are sold alive at markets. To optimize the use of natural resources such as pasture and surface water, whose availability varies throughout the year, livestock farmers are forced to move their herds around: these movements occur all the year round (nomadism) or in specific periods (transhumance). The animal movements vary according to the season and depend on both the availability of resources and other socio-cultural factors, therefore the mobility patterns and the distribution of the volume of animals involved change over the course of the year.  

Temporality, i.e. the variation in time of the mobility network, affects disease spread. Figure 1 – A shows an example of a temporal network and its static counterpart. The network is composed of seven nodes and eight possible links, whose direction is indicated by the arrows. In this case, the temporal network is characterized by three temporal snapshots containing the same nodes but different links. A link that is present and active in a snapshot is not necessarily the same in the previous or the following snapshots. If we disregard the information of timing, we obtain an aggregated/static network composed of the same nodes and links as the temporal network, all present and active at the same time. If we simulate an outbreak in the two networks (temporal and static) (Figure 1 – B), we can see that the potential diffusion of the pathogen differs in the two situations. In this case, there is significantly more propagation in the static network than in the temporal one. This happens because, in the temporal network, the disease can only propagate through temporal paths. In other words, if a link connecting an infected node to a susceptible one is active in a specific temporal snapshot, the disease can spread to the latter, conversely, if the link is not active in the temporal window concerned, the disease propagation stops.

![Figure 1](Figure_1.png)


## Features

The scripts provided in this repository allow to identify the potential nodes that could be reached by a disease at each specific time of the year, taking account of the locations and the activity periods of the sources of infection, as well as the structural changes of the network. To this end we consider a deterministic Susceptible Infected (SI) model/message passing where "infected" nodes can reach only nodes they are in contact with in the specific time interval.

The algorithm relies on the concept of temporal path, i.e. a sequence of links connecting two nodes with each link in the path coming temporally after the one before. When multiple sources are active at the same time, the infection time is estimated as the minimum time span for a time path to connect a source to a node.

Results from the propagation simulation are then plot on a map and colored based on the infection time.

## Content of the repository

The repository contains 3 folders:
  
  * **Data**  containing:
    - [Metadata.xlsx](Data/metadata.xlsx), an excel sheet containing the description for each column of the dataset used for the article;
    - [Dynamic_Network_2020.csv](Data/Dynamic_Network_2020.csv), a mock example of livestock mobility network in Senegal, used for testing the different functions;
    - [Shapefile_WA](Data/Shapefile_WA), a folder containing the shapefiles for the countries in West Africa, used for plotting the results of the mock model.
    
  * **Scripts** containing several R scripts:
    - [Formatting.R](Scripts/Formatting.R), a series of commands to transform raw data in the format needed by the Disease_Propagation functions;
    - [Disease_Propagation.R](Scripts/Disease_Propagation.R), containing the functions to estimate the time to infection on static and temporal network;
    - [Map_Reachability_Infection_Time.R](Scripts/Map_Reachability_Infection_Time.R), containing a function to make a map of the results from the previous step: the Administrative Units are colored based on the arrival time of the disease (infection time);
    - [Example.R](Scripts/Example.R), containing an example showing the steps to follow to produce the maps of reachability and infection time.
  
  * **Outputs** containing some results from the script Example.R: 
    - [MapInfectionTime.png](Outputs/MapInfectionTime.png), an example of maps produced by the script when the sources of infection are localized outside Senegal, and the infection starts in a specific interval;
    - [Propagation_Results.csv](Outputs/Propagation_Results.csv), a dataset containing for each node and each source of infection, the time when the node get infected.


### Description of the function disease_propagation

For each source of an hypothetical disease and each temporal unit of activity of the source (i.e. days/weeks/months/... in which the source is the origin of at least one link), this function provides a list of nodes that can be reached by the disease, the time (in temporal units) needed for the disease to reach the node as well as the number of links needed.
Defined the sources and the interval of interest, the function identifies the nodes and the time of infection from the moment the sources are infected till the maximum time registered in the network data.
In this function, the words "vertex" and "edge" can be used to refer to "node" and "link", respectively.

#### Input
The parameters of the function are:

- netdata: 5 columns dataframe composed by Origin, Destination, Time, Country_or, Country_dest;
- type: string variable with 2 options, "Country" or "Location". "Country" indicates that all possible locations of the chosen country are considered as sources of the disease, "Location" indicates that a set of specific locations are considered as sources of the disease;
- Source: vector containing either the country/countries of the source(s) of the disease (for type = "Country"), or the specific location(s) of the source(s) (for type = "Location");
- onset0: first temporal unit of the studied period;
- terminus0: last temporal unit of the studied period.

```
disease_propagation(netdata, type, Source, onset0, terminus0)
```

The first part of the function formats the network data (netdata) in such a way that it can be used to compute the Temporal Path. 

The script creates a static network,

```
  snet <- network(
    edg,
    vertex.attr = vert,
    vertex.attrnames = c("vertex.id", "Node"),
    directed = TRUE,
    bipartite = FALSE)
```
and a dynamic one.

```
  dnet <- networkDynamic(
    snet,
    edge.spells = dedg,
    vertex.spells = dvert)
```

The sources are selected depending if we know only the countries where the disease comes from (type=="Country" and Source=c(name of the countries)) or if we know the exact locations (type="Location" and Source=c(list of locations)).

```
  sources0<-net %>%
    filter(if(type=="Country") Country_or %in% Source else Origin %in% Source) %>%
    select(Origin) %>%
    left_join(
      vert,
      by = c(Origin = "Node")) %>%
    rename(Node = Origin) %>%
    distinct()
    
  sources<-data.frame(matrix(vector(), 0, 3,
                             dimnames=list(c(), c("Node", "vertex.id", "Time"))),
                      stringsAsFactors=F)
  
  for (x in sources0$vertex.id){
    Time<-dedg$onset[dedg$Origin==x]
    vertex.id<-x
    Node<-sources0$Node[sources0$vertex.id==x]
    temp<-data.frame(Node,vertex.id,Time)
    sources<-rbind(sources,temp)}
  sources<-unique(sources)
```

For each source active during the interval defined by onset0 and terminus0, the Temporal Path (tpath) is estimated. For each node in the tpath, the time when they are infected is estimated (When) and stored, together with the number of the links created between the source(s) and the "infected" node (How_many_links) and some information on the source (Source, Source.id, Time).

```
 for (s in unique(sources$vertex.id)){
    for (w in sources$Time[sources$vertex.id==s]){
      # Only sources activated in the interval determined by onset0 and terminus0 are considered
      if(w %in% seq(onset0:terminus0+1)){ 
      sub1<-subset(dedg,Origin!=s)
      sub2<-subset(dedg,Origin==s & onset==w)
      edgsub<-rbind(sub1,sub2)
      dynnet <- networkDynamic(
        snet,
        edge.spells = edgsub,
        vertex.spells = dvert,
        verbose = FALSE)
      # The function tPath estimates the sequence of nodes and distances that can be reached 
      # from initial vertex (Sources)
      
      Path<-tPath(dynnet,
                  v = s,
                  direction = "fwd",
                  start = w,
                  end = max(netdata$Time)+1, # +1 to take into consideration the links present in the last temporal unit 
                 active.default = F,
                  graph.step.time=1)
                  
      # Sicknodes list of nodes that can be reached in a finite amount of time
      sicknodes<-which(Path$gsteps!=0 & Path$gsteps!=Inf)
      for (n in sicknodes){
        when<-Path$tdist[n]
        links<-Path$gsteps[n]
        temp<-data.frame(Source.id=s,
                         Source=sources$Node[sources$vertex.id==s],
                         Time=w,
                         Which_node=vert$Node[vert$vertex.id==n],
                         Node.id=n,
                         When=when,
                         How_many_links=links)
        results<-unique(rbind(results,temp))
      }
      }
    }
  }
```


#### Output

The output of this function is a 7 columns dataframe:

  - Source.id: id of the source;
  - Source: name of the source, corresponding to its location;
  - Time: moment of activation of the source;
  - Which_node: name of the node reached by the disease, corresponding to its location;
  - Node.id: id of the node reached by the disease;
  - When: time (in temporal units) needed to reach the node;
  - How_many_links: number of links needed to reach the node.

It can be used to produce maps of reachability (which nodes can be reached) and infection time (in how much time/how many links?), through the function map_infection_time.

### Description of the function disease_propagation_AGNET

This function is similar to the previous one, but developed for a static network (no change in the structure).

### Description of the function map_infection_time

This function is used to produce a map where the administrative units are colored based on the expected time of infection and reachability. The reachability is expressed in the map as the presence/absence of the color (absence = gray), while the infection time is expressed by the specific color (shades from red (reached earlier) to yellow (reached later) + green (never reached)).

#### Input
The parameters of the function are:

- data0: dataframe resulting from the disease_propagation function (7 columns dataframe, as explained above);
- shp_dest: shapefile of the country of interest. The shapefile must have the administrative boundaries level chosen as spatial unit (nodes) for the disease_propagation function;
- shp_dest_borders: shapefile of the country of interest, level 0 administrative boundaries;
- shp_neighbors_borders: list of shapefiles of the neighboring countries of the destination country, level 0 administrative boundaries. It is essential to create a list even if there is just one neighboring country;
- breaks: vector of numbers defining the breaks to consider in the choice of the colors for the infection time. 
  Example with data used in the paper (temporal unit = week): Breaks = c(0, 5, 9, Inf) --> 1st break: 0<=x<=5, 2nd break: 5<x<=9, 3rd break: x>9, with x = infection time;
- labels: vector of elements type character explaining the different colors used to describe the infection time, that will be used in the legend. 
  Example with data used in the paper and corresponding to the example for the breaks: Labels = c("One_month", "Two_month", "Later").

```
map_infection_time(data0, shp_dest, shp_dest_borders, shp_neighbors_borders, breaks, labels)
```
#### Output

The function produce a facet plot where each plot contains the result for each starting time. The infection time is colored using categories of interval defined by the user.

## Installation
All the material is freely available by cloning this repository.

```
git clone git@forgemia.inra.fr:umr-astre/sentinel_nodes.git
```

## Usage


The scripts have been developed using R version 3 and 4.
The following packages need to be installed: 


Simulation:

  dplyr
  sna
  tsna
  ndtv
  tidyverse

Plot:

  tmap
  sf
  grid
  paletteer
  
### Example  

**Important Note : if you want to color the map, it is imperative that:**
  
  **The nodes correspond to the administrative units in the map (same level and same name)**
  **You use the shapefile with the correct Administrative Division**

i.e. if nodes represent departments, you need to use a shapefile with the departmental divisions.

An example of usage is shown in the [Example.R](Scripts/Example.R).

#### Importing and formatting the data

```
data_or<-read.csv("../Data/Dynamic_Network_2020.csv",sep=",", fileEncoding = "UTF-8")
```

We format the data for the input, selecting the columns of origin and destination (in this case the origin (or) and destination (dest) are departments), the column containing the timestamp (time) (in this case the weeks when the movements occur), and the columns containing information on the countries of origin and destination, that could be used if we are interested in the introduction of the hypothetical disease from abroad.

```
netdata<-formatting(data0=data_or, or="Dept_or", dest="Dept_dest", time="Week", country_or="Country_or", country_dest="Country_dest")
```

#### Estimation of infection time

We choose the type of analysis we are interested in. 
We select the type indicating that we are interested on the introduction of the hypothetical disease from all the sources in a country --> type = "Country".

```
type<-"Country"
```

We indicate the list of possible countries,

```
sourcesvec<-c("Mauritanie","Mali")
```
and we indicate the interval we are interested in (onset=beginning, terminus=end).

```
onset<-3
terminus<-15
````
Through the disease_propagation function, we can estimate the infection time for each node.

```
results<-disease_propagation(netdata=netdata,type=type, Source=sourcesvec, onset0=onset, terminus0=terminus)
```
#### Plot
We import the national borders of all the countries in the area (in this case West Africa (WA)).
The function takes 2 elements: the path to the directory where all the shapefiles are stored;
the level of administrative unit we are interested in (in this case 0, i.e. national borders).

```
WA_countries<-read_all_gadm("../Data/Shapefile_WA/", 0)
```

We import the national borders of the country of interest as destination of the hypothetical disease (in this case Senegal).

```
Senegal_borders<-WA_countries[["SEN"]]
```

We conduct the analysis at department level (Admin level 2), 

```
WA_depts<-read_all_gadm("../Data/Shapefile_WA/", 2)
```

and the departments of Senegal will color depending on the Infection time. 
It is essential that the administrative level chosen for the shapefile corresponds to the one used in the analysis, as well as the name.

```
Senegal_depts<-WA_depts[["SEN"]]
```
We chose the interval (breaksvec) to categorize outputs (in this case 0-5 and 5-9) and the name to pair (labelsvec).

```
breaksvec<-c(0,5,9)
labelsvec<-c("1 Month","2 Month")
map_infection_time(data0=results, shp_dest=Senegal_depts, shp_dest_borders=Senegal_borders, shp_neighbors_borders=WA_countries, breaks=breaksvec, labels=labelsvec)
````



## Authors and acknowledgment

Codes have been developed by [Alessandra Giacomini-Swansea University](mailto:a.giacomini.2156511@swansea.ac.uk), under the supervision of [Andrea Apolloni-CIRAD](mailto:andrea.apolloni@cirad.fr), [Mamadou Ciss-ISRA](mailto:ciss.mamadou@gmail.com), [Facundo Muñoz-CIRAD](mailto:facundo.munoz@cirad.fr).

This work has been supported by the Project Eco-PPR (European Commission through the International Fund for Agricultural Development (grant number 2000002577) and the CGIAR Livestock research program, RVF OIE twinning program (CIRAD-ISRA) granted through the EBO-SURSY project (European Union FOOD/2016/379-660). 

## Citation

Please if you like and use these scripts, cite us:

Ciss, M. et al. (2023) ‘Description of the cattle and small ruminants trade network in Senegal and implication for the surveillance of animal diseases’. arXiv. Available at: https://doi.org/10.48550/arXiv.2301.11784.


## License

GNU LESSER GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

 
